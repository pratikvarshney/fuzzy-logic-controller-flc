/*
 *  Author  :   Pratik Varshney
 *  Date    :   05/04/2015
 *  Program :   Fuzzy Logic Controller (FLC)
 */

#include<stdio.h>
#include<string.h>

#define NUMVARS 3
#define N 0
#define Z 1
#define P 2
#define NUMRULES (NUMVARS*NUMVARS)

#define MINVAL -3
#define MAXVAL 3
#define RANGE ((MAXVAL)-(MINVAL)+1)
#define indexof(x) (x-(MINVAL))

#define min(a,b) (a<b)?a:b
#define max(a,b) (a>b)?a:b

char label[NUMVARS][2]={"N", "Z", "P"};

int show_du_table(double du[NUMRULES][RANGE], int rows, int cols);
int show_membership_table(double membership[NUMVARS][RANGE], int rows, int cols);
int show_rulebase(int rulebase[NUMVARS][NUMVARS], int rows, int cols);
int show_result(double result[RANGE][RANGE], int rows, int cols);

int main(int argc, char *argv[])
{
    int ek, cek;
    double membership[NUMVARS][RANGE]={     {1.0, 0.6, 0.4, 0.0, 0.0, 0.0, 0.0},
                                            {0.0, 0.4, 0.6, 1.0, 0.6, 0.4, 0.0},
                                            {0.0, 0.0, 0.0, 0.0, 0.4, 0.6, 1.0}
                                            };
    double du[NUMRULES][RANGE];
    double mu_e, mu_ce;
    double temp_max[RANGE];
    double result[RANGE][RANGE];
    int i, j, l, s;
    double min_val, max_val, numerator, denominator;

    /* Set program debug mode */
    int sh_du_table = 0;
    if(argc>1)
    {
        if(strcmp(argv[1], "--debug")==0)
        {
            sh_du_table = 1;
        }
    }
    /* END Set program debug mode */

    int rulebase[NUMVARS][NUMVARS]={    {N,N,Z},
                                        {N,Z,P},
                                        {Z,P,P}
                                        };

    show_membership_table(membership, NUMVARS, RANGE);

    show_rulebase(rulebase, NUMVARS, NUMVARS);

    /* STEP 1 : For each measured value of ek and cek on UoD E and CE. */
    for(ek=MINVAL; ek<=MAXVAL; ek++)
    {
        for(cek=MINVAL; cek<=MAXVAL; cek++)
        {
            /* STEP 2 : Define a 2D array to store DU[j,l][s] */
            for(i=0; i<NUMRULES; i++)
            {
                for(j=0; j<RANGE; j++)
                {
                    du[i][j] = 0.0;
                }
            }

            /* STEP 3 : For each row index of rulebase, j=0,1,... */
            for(j=0; j<NUMVARS; j++)
            {
                /* STEP 3.1 : For each column index of rulebase, l=0,1,... */
                for(l=0; l<NUMVARS; l++)
                {
                    /* STEP 3.1.1 : Read the consequent fuzzy membership vector DU[j,l][s]. */
                    /* Note : I've skipped this step here. I'll read the values in step 3.1.4 */

                    i=(j*NUMVARS)+l; /* Now i = [j,l] */

                    /* STEP 3.1.2 : Read the membership value from the fuzzy subsets. */
                    mu_e = membership[j][indexof(ek)];
                    mu_ce = membership[l][indexof(cek)];

                    /* STEP 3.1.3 : Find minimum of the two membership values. */
                    min_val = min(mu_e, mu_ce);

                    /* STEP 3.1.4 : For each support element 's' find minimum of the values found in 3.1.3
                    and the membership value of the consequent DU[j,l][s] of the rule R[j][l], and restore
                    in DU[j,l][s]. */
                    for(s=0; s<RANGE; s++)
                    {
                        du[i][s] = min(membership[rulebase[j][l]][s], min_val);
                    }
                }
            }

            /* STEP 4 : For each support element in DU[j,l][s], find maximum element of all rows. */
            for(s=0; s<RANGE; s++)
            {
                max_val = du[0][s];
                for(i=0; i<NUMRULES; i++)
                {
                    max_val = max(du[i][s], max_val);
                }
                temp_max[s] = max_val;
            }

            /* STEP 5 : Calculate du(k) */
            numerator = 0;
            denominator = 0;
            for(i=MINVAL; i<=MAXVAL; i++)
            {
                numerator += (i*temp_max[indexof(i)]);
                denominator += (temp_max[indexof(i)]);
            }

            result[indexof(ek)][indexof(cek)] = numerator/denominator;

            if(sh_du_table)
            {
                printf("\n================================================================================");

                printf("\n\nFor e=%d and ce=%d\n", ek, cek);
                show_du_table(du, NUMRULES, RANGE);

                printf("Max\t|\t");
                for(s=0; s<RANGE; s++) printf("%.1lf\t", temp_max[s]);
                printf("\n--------------------------------------------------------------------------------");
                printf("\n\ndu = %.1lf\n", result[indexof(ek)][indexof(cek)]);
            }
        }
    }

    show_result(result, RANGE, RANGE);

    fflush(stdin);
    fprintf(stderr, "\nPress Enter key to EXIT ...");
    getchar();

    return 0;
}

int show_du_table(double du[NUMRULES][RANGE], int rows, int cols)
{
    int i, j;
    printf("\n--------------------------------------------------------------------------------");
    printf("\n                                    DU TABLE");
    printf("\n--------------------------------------------------------------------------------");
    printf("\n\t|\t");
    for(j=MINVAL; j<=MAXVAL; j++) printf("(%d)\t", j);
    printf("\n--------+-----------------------------------------------------------------------");
    printf("\n \t|");
    for(i=0; i<rows; i++)
    {
        printf("\nRule %d)\t|\t", i+1);
        for(j=0; j<cols; j++)
        {
            printf("%.1lf\t", du[i][j]);
        }
        printf("\n \t|");
    }
    printf("\n--------------------------------------------------------------------------------");
    printf("\n");
    return 0;
}

int show_membership_table(double membership[NUMVARS][RANGE], int rows, int cols)
{
    int i, j;
    printf("\n--------------------------------------------------------------------------------");
    printf("\n                                MEMBERSHIP TABLE");
    printf("\n--------------------------------------------------------------------------------");
    printf("\n\t|\t");
    for(j=MINVAL; j<=MAXVAL; j++) printf("(%d)\t", j);
    printf("\n--------+-----------------------------------------------------------------------");
    printf("\n \t|");
    for(i=0; i<rows; i++)
    {
        printf("\n(%s)\t|\t", label[i]);
        for(j=0; j<cols; j++)
        {
            printf("%.1lf\t", membership[i][j]);
        }
        printf("\n \t|");
    }
    printf("\n--------------------------------------------------------------------------------");
    printf("\n");
    return 0;
}

int show_rulebase(int rulebase[NUMVARS][NUMVARS], int rows, int cols)
{
    int i, j;
    printf("\n==============================");
    printf("\n          RULEBASE");
    printf("\n------------------------------");
    printf("\n%s", "   E\\CE  |");
    for(i=0; i<cols; i++) printf("%5s", label[i]);
    printf("\n---------+--------------------");
    for(i=0; i<rows; i++)
    {
        printf("\n%5s%5s", label[i], "|");
        for(j=0; j<cols; j++)
        {
            printf("%5s", label[rulebase[i][j]]);
        }
    }
    printf("\n==============================\n\n");

    return 0;
}

int show_result(double result[RANGE][RANGE], int rows, int cols)
{
    int i, j;
    printf("\n--------------------------------------------------------------------------------");
    printf("\n                                     RESULT");
    printf("\n--------------------------------------------------------------------------------");
    printf("\nE\\CE\t|\t");
    for(j=MINVAL; j<=MAXVAL; j++) printf("(%d)\t", j);
    printf("\n--------+-----------------------------------------------------------------------");
    printf("\n\t|\t");
    for(i=0; i<rows; i++)
    {
        printf("\n(%d)\t|\t", i+MINVAL);
        for(j=0; j<cols; j++)
        {
            printf("%.1lf\t", result[i][j]);
        }
        printf("\n\t|");
    }
    printf("\n--------------------------------------------------------------------------------");
    printf("\n");
    return 0;
}
